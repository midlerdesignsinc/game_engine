import _get from "lodash.get";
import Chance from "chance";

const chance = new Chance();

export default class GameEngine {
  constructor ({ appWindowId, showGrid }) {
    this.appWindow = document.getElementById(appWindowId);
    this.scene = {
      name: "",
      config: {},
      layers: [],
      objects: [],
    }
    this.showGrid = showGrid;
    this.numberOfCells = 20;
    this.player = null;

    this.renderScene({
      sceneName: "title",
      playerStartX: null,
      playerStartY: null,
      playerStartZ: null,
    });
  }

  createNewLayer () {
    const newCanvasEle = document.createElement("canvas");
    newCanvasEle.id = `${this.scene.name}_${chance.guid()}`;
    newCanvasEle.classList.add("canvas-layer");
    newCanvasEle.width = this.appWindow.offsetWidth;
    newCanvasEle.height = this.appWindow.offsetHeight;
    newCanvasEle.style["background-color"] = "transparent";

    this.appWindow.appendChild(newCanvasEle);

    return newCanvasEle;
  }

  renderScene ({ sceneName }) {
    // parse the json file and set the scene details
    const sceneConfig = require(`./scenes/${sceneName}/config.json`);
    this.scene.name = _get(sceneConfig, "name", "");
    this.scene.config = sceneConfig;

    // render the scene layers
    if(sceneConfig.hasOwnProperty("layers")) for (let i in sceneConfig.layers) {
      const layerConfig = sceneConfig.layers[i];
      this.renderLayer(layerConfig, i);
    }

    // render the scene objects
    if(sceneConfig.hasOwnProperty("objects")) for (const objectConfig of sceneConfig.objects) {
      this.renderObject(objectConfig, null);
    }

    // if show grid set to true create a grid layer
    if (this.showGrid) this.renderGrid();

    // find the player obj and set it to the class instance if applicable
    if (this.player) this.player.init({
      spriteSet: sceneConfig.type,
      numberOfCells: this.numberOfCells,
      layers: this.scene.layers
    });

    // start keyboard listening
    this.toggleKeyboardListener();
  }

  renderLayer (layerConfig, zIndex) {
    // grab some config stuff
    const bgColor = _get(layerConfig, "bgColor", "transparent");
    const bgImage = _get(layerConfig, "bgImage", null);
    const tiles = _get(layerConfig, "tiles", []);

    // create a canvas element and set some stylings
    const newCanvasEle = this.createNewLayer();
    newCanvasEle.style["background-color"] = bgColor;
    newCanvasEle.style["z-index"] = zIndex;
    if (bgImage) {
      newCanvasEle.style["background-image"] = `url('scenes/${this.scene.name}/images/${bgImage}')`;
      newCanvasEle.style["background-position"] = "center";
      newCanvasEle.style["background-repeat"] = "no-repeat";
      newCanvasEle.style["background-size"] = "100%, 100%";
    }

    // add the layer object to the current scene
    this.scene.layers.push({
      id: newCanvasEle.id,
      bgColor,
      bgImage,
      ele: newCanvasEle,
      tiles
    });
  }

  renderObject (objectInstance) {
    const { startingX:x, startingY:y, startingZ:z, startingDirection:direction, handle } = objectInstance;
    const objectConfig = require(`./objects/${handle}/config.json`);

    switch (objectConfig.type) {
      case "player":
        const newObj = new Player({
          id: chance.guid(),
          config: objectConfig,
          canvas: this.createNewLayer(),
          handle,
          x,
          y,
          z,
          direction,
        });

        this.player = newObj;
      break;
    }
  }

  renderGrid () {
    // create a canvas element
    const newCanvasEle = document.createElement("canvas");
    newCanvasEle.classList.add("canvas-layer");
    newCanvasEle.style["background-color"] = "transparent";
    newCanvasEle.style["z-index"] = 99;

    // add the canvas element to the app window
    this.appWindow.appendChild(newCanvasEle);

    const ctx = newCanvasEle.getContext("2d");
    ctx.beginPath();
    const colSize = newCanvasEle.width / this.numberOfCells;
    const rowSize = newCanvasEle.height / this.numberOfCells;

    for (let i = 0;i < this.numberOfCells; i++) {
      ctx.moveTo((i*colSize), 0);
      ctx.lineTo((i*colSize), newCanvasEle.height);
    }
    for (let i = 0;i < this.numberOfCells; i++) {
      ctx.moveTo(0, (i*rowSize));
      ctx.lineTo(newCanvasEle.width, (i*rowSize));
    }
    ctx.stroke();

    for (let i = 0;i < (this.numberOfCells * this.numberOfCells); i++) {
      if (i < ((this.numberOfCells * 2) -5)){
        // alert(i);
        // alert(colSize * i);
      }
      ctx.font = "8px Arial";
      ctx.fillStyle = "white";
      ctx.fillText(String(i), (colSize * i) - (colSize * Math.floor(i / this.numberOfCells) * this.numberOfCells), rowSize * (1+ Math.floor(i / this.numberOfCells)));
    }
  }

  toggleKeyboardListener () {
    const self = this;
    document.addEventListener("keydown", function (event) {
      if (event.code.startsWith("Arrow")) self.movePlayer(event.code);
    });
  }

  movePlayer (keyCode) {
    if (this.player) {
      switch (keyCode) {
        case "ArrowUp": this.player.moveUp(); break;
        case "ArrowDown": this.player.moveDown(); break;
        case "ArrowLeft": this.player.moveLeft(); break;
        case "ArrowRight": this.player.moveRight(); break;
      }
    }
  }
}

class Player {
  constructor ({ id, config, canvas, handle, x, y, z, direction }) {
    this.id = id;
    this.config = config;
    this.canvas = canvas;
    this.x = x;
    this.y = y;
    this.z = z;
    this.direction = direction;
    this.sprite = {
      img: null,
      frameWidth: null,
      frameHeight: null,
      posX: 0,
      posY: 0,
    };
    this.handle = handle;
    this.numberOfCells = null;
    this.ready = true;
    this.layers = [];

    // set the canvas layer z-index
    this.canvas.style["z-index"] = z;
  }

  init ({ spriteSet, numberOfCells, layers }) {
    // prep the sprite sheet
    const { frameWidth, frameHeight, src } = this.config.sprite_sets[spriteSet];
    this.sprite.frameWidth = frameWidth;
    this.sprite.frameHeight = frameHeight;
    this.sprite.img = new Image();
    this.sprite.src = src;
    this.numberOfCells = numberOfCells;
    this.layers = layers;

    this.sprite.img.onload = () => {
      this.draw();
    }

    this.sprite.img.src = `objects/${this.handle}/sprites/${this.sprite.src}`;

    return this;
  }

  draw () {
    const colSize = this.canvas.width / this.numberOfCells;
    const rowSize = this.canvas.height / this.numberOfCells;

    const ctx = this.canvas.getContext("2d");
    ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

    ctx.drawImage(
      this.sprite.img,
      (this.sprite.posX * this.sprite.frameWidth), this.sprite.posY * this.sprite.frameHeight, this.sprite.frameWidth, this.sprite.frameHeight,
      this.x * colSize, (this.y -1) * rowSize, colSize, rowSize * 2
    );
  }

  getTilePosition (x, y) {
    return (y * this.numberOfCells) + x;
  }

  moveUp () {
    if (this.ready) {
      this.sprite.posY = 1;
      this.draw();

      const targetTilePos = this.getTilePosition(this.x, this.y - 1);
      const tileConfig = this.layers[this.z].tiles[targetTilePos];

      if (this.y > 0 && tileConfig.state !== "solid") {
        this.ready = false;
        this.animateMovement({ axis: "y", tiles: -1 });
      }
    }

  }

  moveDown () {
    if (this.ready) {
      this.sprite.posY = 0;
      this.draw();

      const targetTilePos = this.getTilePosition(this.x, this.y + 1);
      const tileConfig = this.layers[this.z].tiles[targetTilePos];

      if (this.y < (this.numberOfCells - 1) && tileConfig.state !== "solid") {
        this.ready = false;
        this.animateMovement({ axis: "y", tiles: 1 });
      }
    }
  }

  moveLeft () {
    if (this.ready) {
      this.sprite.posY = 2;
      this.draw();

      const targetTilePos = this.getTilePosition(this.x - 1, this.y);
      const tileConfig = this.layers[this.z].tiles[targetTilePos];

      if (this.x > 0 && tileConfig.state !== "solid") {
        this.ready = false;
        this.animateMovement({ axis: "x", tiles: -1 });
      }
    }
  }

  moveRight () {
    if (this.ready) {
      this.sprite.posY = 3;
      this.draw();

      const targetTilePos = this.getTilePosition(this.x + 1, this.y);
      const tileConfig = this.layers[this.z].tiles[targetTilePos];

      if (this.x < (this.numberOfCells - 1) && tileConfig.state !== "solid") {
        this.ready = false;;
        this.animateMovement({ axis: "x", tiles: 1 });
      }
    }
  }

  animateMovement ({ axis, tiles, currentFrame }) {
    let loop = true;
    const frameRate = 8;

    if (currentFrame && currentFrame < frameRate) {
      let posX = this.sprite.posX;
      this.sprite.posX = posX === 3 ? 0 : posX + 1;
      this[axis] += (tiles / frameRate);
      this.draw();
      currentFrame++;
    }
    else if (currentFrame && currentFrame >= frameRate) {
      let posX = this.sprite.posX;
      this.sprite.posX = posX === 3 ? 0 : posX + 1;
      this[axis] += (tiles / frameRate);
      this.draw();
      loop = false;
      this.ready = true;
    }
    else {
      currentFrame = 2;
      let posX = this.sprite.posX;
      this.sprite.posX = posX === 3 ? 0 : posX + 1;
      this[axis] += (tiles / frameRate);
      this.draw();
    }

    if (loop) setTimeout(() => {
      this.animateMovement({
        axis, tiles, currentFrame
      });
    }, 25);
  }
}
