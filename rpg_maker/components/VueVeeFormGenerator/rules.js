import { extend } from 'vee-validate';
import { required, email } from "vee-validate/dist/rules";
const units = [
  "teaspoon",
  "teaspoons",
  "tsp",
  "tablespoon",
  "tablespoons",
  "tbl",
  "tbls",
  "tbsp",
  "fluid-ounce",
  "fluid-ounces",
  "fl-oz",
  "cup",
  "cups",
  "pint",
  "pints",
  "pt",
  "quart",
  "quarts",
  "qt",
  "gallon",
  "gallons",
  "gal",
  "milliliter",
  "milliliters",
  "ml",
  "millilitre",
  "millilitres",
  "liter",
  "liters",
  "litre",
  "litres",
  "deciliter",
  "deciliters",
  "decilitre",
  "decilitres",
  "dl",
  "pound",
  "pounds",
  "milligram",
  "milligrams",
  "milligramme",
  "milligrammes",
  "gram",
  "grams",
  "gramme",
  "grammes",
  "g",
  "kilogram",
  "kilograms",
  "kilogramme",
  "kilogrammes",
  "kg",
  "ounce",
  "ounces",
  "oz"
];

extend("required", {
  ...required,
  message: "* This field is required"
});

extend("email", {
  ...email,
  message: "* Invalid email address"
});

extend("serving_size", {
  message: "* Invalid amount/measurement",
  validate: value => {
    const patternMatch = value.match(/^\d+((\.|\/)\d+?)?\s.+$/);

    if (patternMatch) {
      const unit = value.split(" ")[1].toLowerCase();
      if (units.indexOf(unit) !== -1) return true;
    }

    return false;
  }
});

extend("total_per_unit", {
  message: "* Invalid calories/unit",
  validate: value => {
    const patternMatch = value.match(/^(\d+)?(\.\d+)?\s\bper\b\s(\d+)?(\.|\/)?\d+\s.+$/);

    if (patternMatch) {
      const unit = value.split(" ")[3].toLowerCase();
      if (units.indexOf(unit) !== -1) return true;
    }

    return false;
  }
});
