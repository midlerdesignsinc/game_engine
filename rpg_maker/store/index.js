export const state = () => ({
  currentProject: null,
})

export const mutations = {
  setCurrentProject (state, value) {
    state.currentProject = value;
  },
}

export const actions = {
  async loadProject (state, projectName) {
    const scenes = await this.dispatch('getProjectScenes', projectName);

    this.commit("setCurrentProject", {
      name: projectName,
      scenes,
    });
  },

  async unLoadProject (state) {
    this.commit("setCurrentProject", null);
  },

  async getProjectScenes (state, projectName) {
    console.log(projectName);
    const httpResponse = await this.$axios({
      method: 'post',
      url: '/api/glob',
      data: {
        dirPath: `static/game_projects/${projectName}/scenes`,
        fileExt: ""
      }
    });
    return httpResponse.data;
  }
}


