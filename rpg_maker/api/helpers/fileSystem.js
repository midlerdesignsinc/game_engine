import fs from "fs";
import path from "path";
import glob from "glob";

export default class FileSystemHelper {
  constructor() {}

  glob (dirPath, fileExt) {
    return new Promise((res, rej) => {
      glob(`${dirPath}/*${fileExt}`, function (err, files) {
        if (err) console.log(err);
        else res(files);
      });
    });
  }

  readFile (filePath) {
    return fs.readFileSync(filePath,'utf8');
  }

  saveFile (filePath, data) {
    return fs.writeFileSync(filePath, data);
  }

  deleteFile (filePath) {
    return fs.unlinkSync(filePath);
  }
}
