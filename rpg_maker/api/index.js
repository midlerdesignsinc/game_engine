import express from "express";
import session from "express-session";
const _get = require("lodash.get");
const Chance = require("chance");

const chance = new Chance();
const router = express.Router();
const app = express();

import FileSystemHelper from "./helpers/fileSystem";
const fsUtils = new FileSystemHelper();

const url = "mongodb://localhost:27017";

router.use((req, res, next) => {
  Object.setPrototypeOf(req, app.request);
  Object.setPrototypeOf(res, app.response);
  req.res = res;
  res.req = req;
  next();
});

router.post("/glob", async (req, res, next) => {
  const files = await fsUtils.glob(req.body.dirPath, req.body.fileExt);
  return res.status(201).json(files);
});

router.post("/read_file", async (req, res, next) => {
  const fileData = await fsUtils.readFile(req.body.filePath);
  return res.status(201).json(fileData);
});

/*
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% MAP ROUTES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 */
router.put("/projects/:projectName/maps/:mapName", async (req, res, next) => {
  try {
    const { mapData, layerImgs } = req.body;
    const mapPath = `static/game_projects/${req.params.projectName}/scenes/${req.params.mapName}/data.json`;
    const mapImgPath = `static/game_projects/${req.params.projectName}/scenes/${req.params.mapName}/images`;
    const mapWrite = await fsUtils.saveFile(mapPath, mapData);

    for (let i in layerImgs) {
      const imgData = layerImgs[i].replace(/^data:image\/\w+;base64,/, "");
      const buf = new Buffer(imgData, 'base64');

      const imgWrite = await fsUtils.saveFile(`${mapImgPath}/layer_${i}.webp`, buf);
    }

    return res.status(201).send();
  } catch (err) {
    console.log(err);
    return res.status(500).send();
  }
});

router.delete("/projects/:projectName/maps/:mapName/images/:imgName", async (req, res, next) => {
  try {
    const mapPath = `static/game_projects/${req.params.projectName}/scenes/${req.params.mapName}/data.json`;
    const mapImgPath = `static/game_projects/${req.params.projectName}/scenes/${req.params.mapName}/images/${req.params.imgName}`;
    const mapImgDel = await fsUtils.deleteFile(mapImgPath);

    return res.status(201).send();
  } catch (err) {
    console.log(err);
    return res.status(500).send();
  }
});


// Export the server middleware
export default {
  path: "/api",
  handler: router,
};
