import bodyParser from "body-parser";
import session from "express-session";

export default {
  mode: "universal",
  target: "server",
  head: {
    title: "RPG Maker Demo",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: process.env.npm_package_description || "" }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      { rel: "stylesheet", type: "text/css", href: "https://fonts.googleapis.com/css2?family=Oregano&display=swap"},
      { rel: "stylesheet", type: "text/css", href: "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"}
    ],
    script: [
      { src: 'https://code.jquery.com/jquery-3.5.1.slim.min.js', integrity: "sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj",  crossorigin: "anonymous" },
      { src: 'https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js', integrity: "sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN",  crossorigin: "anonymous" },
      { src: 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', integrity: "sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl",  crossorigin: "anonymous" },
    ]
  },
  css: [
  ],
  plugins: [
    { src: "~/plugins/vue-toast-notification.js" },
    { src: '~/plugins/vuex-persist.js', ssr: false }
  ],
  components: true,
  buildModules: [],
  serverMiddleware: [
    bodyParser.json({
      limit: '5mb'
    }),
    session({
      secret: "TQBFJ0TM0@H3$CTBHRLC@WHGH@P4A(NS145",
      cookie: {
        path: "/",
        maxAge: 900000,
        httpOnly: true
      }
    }),
    "~/api",
  ],
  axios: {
    BaseURL: 'http://localhost:3001',
    proxy: true,
  },
  modules: [
    "@nuxtjs/axios",
  ],
  build: {},
  server: {
    port: 3001
  }
}
